/**
 * Responsive Modal component
 *
 * @url         --
 * @author      Vladimir Bujanovic
 * @version     0.4
 *
 * @require core/App.js
 * @require core/App.Component.js
 */

;
(function (App, $, undefined) {
    var Component = App.Core.Component,
        Helpers = App.Helpers;

    App.Components.Modal = Component.extend({
        wrapper: '#modal',
        trigger: '.modal-btn',
        parent: '#page-content',
        maxWidth: 800,
        minWidth: 400,
        speed: 400,
        onShow: $.noop,
        onClose: $.noop,
        getData: $.noop,
        submitForm: $.noop,

        render: function () {
            var htmlClose = '<span class="modal-close"></span>';

            this.$inner = $('<div class="modal-inner-wrapper"></div>');
            this.$mask = $('<div class="modal-mask"></div>');

            this.$wrapper
                .css({
                    minWidth: this.minWidth,
                    maxWidth: this.maxWidth
                })
                .append(htmlClose)
                .append(this.$inner)
                .append(this.$mask);

        },

        bindEvents: function () {
            var base = this;

            base.$wrapper
                .on('click', ':reset', $.proxy(base.close, base))

                .on('click', ':submit', $.proxy(base.submitForm, base))

                .on('click', '.modal-close', $.proxy(base.close, base))

                .on('click', '.modal-mask', $.proxy(base.close, base));

            base.$parent
                .on('click', this.trigger, function (e) {
                    e.preventDefault();
                    var url = $(e.currentTarget).attr('href');

                    $.ajax({
                        type: "GET",
                        url: url,
                        cache: false,
                        async: false,
                        success: function (response) {
                            base.open(response);
                        },
                        error: function (error) {
                            console.error(error);
                        }
                    });
                });

            $(window).resize(function () {
                base.centerDialog();
            });

            $(document).keyup(function (e) {
                // esc key pressed
                if (e.keyCode == 27) {
                    base.close();
                }
            });
        },

        centerDialog: function () {
            var base = this,
                ww = $(window).width(),
                wh = $(window).height();

            base.$wrapper.css({
                left: (ww - base.$wrapper.width()) / 2,
                top: (wh - base.$wrapper.height()) / 2 + $(window).scrollTop()
            });

            base.$mask.css({
                width: ww,
                height: wh
            });
        },

        open: function (html) {
            var base = this;

            base.$inner.html(html);
            base.$mask.show();
            base.centerDialog();
            base.$wrapper.fadeIn(base.speed, function () {
                base.onShow.call(base);
            });

            return base;
        },

        close: function (data) {
            var base = this;

            base.$wrapper.fadeOut(base.speed, function () {
                base.$inner.html('');
                base.$mask.hide();
                base.onClose.call(base, data);
            });

            return base;
        },

        setElement: function () {

            if (this.wrapper) {
                this.$wrapper = this.wrapper instanceof jQuery ? this.wrapper : $(this.wrapper);
            }
            if (this.parent) {
                this.$parent = this.parent instanceof jQuery ? this.parent : $(this.parent);
            }
        }

    });

})(App || {}, jQuery, undefined);

