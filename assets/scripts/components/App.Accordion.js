/**
 * Responsive Accordion component
 *
 * @url         --
 * @author      Vladimir Bujanovic
 * @version     0.4
 *
 * @require core/App.js
 * @require core/App.Component.js
 */

;
(function (App, $, undefined) {
    var Component = App.Core.Component,
        Helpers = App.Helpers;

    App.Components.Accordion = Component.extend({
        wrapper : '.accordion',
        trigger : 'dt',
        control : 'dd',
        speed : 400,

        bindEvents : function () {
            this.$wrapper.on('click', this.trigger, $.proxy(this.animateContent, this));
        },

        animateContent : function (e) {

            var $this = $(e.currentTarget);

            this.$wrapper.find(this.trigger).removeClass('active');
            this.$wrapper.find(this.control).slideUp(this.speed);

            $this.addClass('active').next().slideDown(this.speed);
        }
    });

})(App || {}, jQuery, undefined);

