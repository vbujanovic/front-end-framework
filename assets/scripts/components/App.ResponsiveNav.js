/**
 * Responsive Navigation component
 *
 * @url         --
 * @author      Vladimir Bujanovic
 * @version     0.4
 *
 * @require core/App.js
 * @require core/App.Component.js
 */

;
(function (App, $, undefined) {
    var Component = App.Core.Component,
        Helpers = App.Helpers,
        Constants = App.Constants;

    App.Components.ResponsiveNav = Component.extend({
        wrapper: '#res-nav',
        trigger: '#pull-btn',
        width: 767,
        onOpen: $.noop,
        onClose: $.noop,

        render: function () {
            var base = this;

            base.$list = this.$wrapper.find('ul');

            if (Modernizr.csstransitions) {
                base.$list.css({
                    'max-height': 0,
                    'display': 'block'
                });
            }

            return base;
        },

        bindEvents: function () {
            var base = this;

            base.$wrapper.on('touchstart click', base.trigger, $.proxy(base.toggle, this));

            $(window).resize(function () {
                var w = $(window).width();

                if (w > base.width && base.$list.is(':hidden')) {
                    base.$list.removeAttr('style');
                }
            });
        },

        toggle: function (e) {
            var base = this;

            e.preventDefault();

            if (base.$wrapper.hasClass('expanded')) {
                base.close();
            } else {
                base.open();
            }
        },

        open: function () {
            var base = this;

            if (Modernizr.csstransitions) {

                base.$list
                    .css('max-height', 1000)
                    .css(Constants.Prefix.js + 'Transition', "max-height 1.0s ease-in-out");

            } else {
                base.$list.slideDown();
            }

            base.$wrapper.addClass('expanded');
            //Callback
            base.onOpen.call(base);

            return base;
        },

        close: function () {
            var base = this;

            if (Modernizr.csstransitions) {

                base.$list
                    .css('max-height', 0)
                    .css(Constants.Prefix.js + 'Transition', "max-height 1.0s ease-in-out");

            } else {
                base.$list.slideUp();
            }

            base.$wrapper.removeClass('expanded');
            //Callback
            base.onClose.call(base);

            return base;
        }

    });

})(App || {}, jQuery, undefined);

