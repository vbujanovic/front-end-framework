/**
 * Responsive Tabs component
 *
 * @url         --
 * @author      Vladimir Bujanovic
 * @version     0.4
 *
 * @require core/App.js
 * @require core/App.Component.js
 */
;
(function (App, $, undefined) {
    var Component = App.Core.Component,
        Helpers = App.Helpers;

    App.Components.Tabs = Component.extend({
        wrapper: '#tabs',
        nav: 'li',
        content: '.tab',
        activeCssClass: 'active',
        animated: true,
        onBeforeChange: $.noop,
        onAfterChange: $.noop,

        render: function () {
            if (this.animated) {
                this.$content.addClass('animated');
            }

            var tabName = window.location.hash.slice(1).length > 0 ?
                          window.location.hash.slice(1) :
                          this.$content.eq(0).attr('data-tab');

            this.getTabNames();
            this.changeTab(tabName);
        },

        getTabNames: function () {
            var base = this;

            this.tabs = [];
            this.$nav.each(function () {
                base.tabs.push($(this).data('tab'));
            });
        },


        bindEvents: function () {
            var base = this;

            this.$wrapper.on('click', base.nav, function (e) {
                e.preventDefault();

                base.changeTab($(this).data('tab'));
            });

        },

        changeTab: function (tabName) {
            //On before change callback
            this.onBeforeChange.call(this, tabName);

            this.$nav.removeClass(this.activeCssClass);
            this.$content.removeClass(this.activeCssClass);

            if (tabName && $.inArray(tabName, this.tabs) > -1) {
                this.$nav.filter('[data-tab="' + tabName + '"]').addClass(this.activeCssClass);
                this.$content.filter('[data-tab="' + tabName + '"]').addClass(this.activeCssClass);
            }
            else {
                this.$nav.eq(0).addClass(this.activeCssClass);
                this.$content.eq(0).addClass(this.activeCssClass);
            }

            window.location.hash = tabName;

            //On after change callback
            this.onAfterChange.call(this, tabName);
        },

        setElement: function () {
            if (this.wrapper) {
                this.$wrapper = this.wrapper instanceof jQuery ? this.wrapper : $(this.wrapper);
                this.$nav = this.$wrapper.find(this.nav);
                this.$content = this.$wrapper.find(this.content);
            }
        }

    });

})(App || {}, jQuery, undefined);

