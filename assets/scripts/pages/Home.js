/**
 * Home page View
 *
 * @url         --
 * @author      Vladimir Bujanovic
 * @version     0.2
 *
 * @require core/App.js
 * @require core/App.View.js
 * @require modules/*.js
 */

;
(function (App, $) {
    var Core = App.Core,
        Page = Core.Page,
        Components = App.Components,
        Instances = App.Instances,
        Helpers = App.Helpers;

    App.Pages.Home = Page.extend({

        initialize: function () {

            Instances.HomeAccordion = Components.Accordion.extend({
                wrapper : '#accordion'
            }).initialize();

            Instances.Modal = Object.create(Components.Modal).initialize();

            Instances.MainTabs = Components.Tabs.extend({
                wrapper: '#tabs'
            }).initialize();

        }

    });

})(App || {}, jQuery);