/**
 * jQuery gMap - Google Maps API V3
 *
 * @url        http://github.com/marioestrada/jQuery-gMap
 * @author    Mario Estrada <me@mario.ec> based on original plugin by Cedric Kastner <cedric@nur-text.de>
 * @version    2.1.2
 */
(function ($) {
    // Main plugin function
    $.fn.gMap = function (options, methods_options) {
        // Optional methods
        switch (options) {
            case 'addMarker':
                return $(this).trigger('gMap.addMarker', [methods_options.latitude, methods_options.longitude, methods_options.content, methods_options.icon, methods_options.popup]);
            case 'centerAt':
                return $(this).trigger('gMap.centerAt', [methods_options.latitude, methods_options.longitude, methods_options.zoom]);
        }

        // Build main options before element iteration
        var opts = $.extend({}, $.fn.gMap.defaults, options);

        // Iterate through each element
        return this.each(function () {
            // Create map and set initial options
            var $gmap = new google.maps.Map(this);

            // Create new object to geocode addresses
            var $geocoder = new google.maps.Geocoder();

            // Check for address to center on
            if (opts.address) {
                // Get coordinates for given address and center the map
                $geocoder.geocode({
                        address : opts.address
                    }, function (gresult, status) {
                    if (gresult && gresult.length) {
                        $gmap.setCenter(gresult[0].geometry.location);
                    }
                    });
            } else {
                // Check for coordinates to center on
                if (opts.latitude && opts.longitude) {
                    // Center map to coordinates given by option
                    $gmap.setCenter(new google.maps.LatLng(opts.latitude, opts.longitude));
                } else {
                    // Check for a marker to center on (if no coordinates given)
                    if ($.isArray(opts.markers) && opts.markers.length > 0) {
                        // Check if the marker has an address
                        if (opts.markers[0].address) {
                            // Get the coordinates for given marker address and center
                            $geocoder.geocode({
                                    address : opts.markers[0].address
                                }, function (gresult, status) {
                                if (gresult && gresult.length > 0) {
                                    $gmap.setCenter(gresult[0].geometry.location);
                                }
                                });
                        } else {
                            // Center the map to coordinates given by marker
                            $gmap.setCenter(new google.maps.LatLng(opts.markers[0].latitude, opts.markers[0].longitude));
                        }
                    } else {
                        // Revert back to world view
                        $gmap.setCenter(new google.maps.LatLng(34.885931, 9.84375));
                    }
                }
            }
            $gmap.setZoom(opts.zoom);

            // Set the preferred map type
            $gmap.setMapTypeId(google.maps.MapTypeId[opts.maptype]);

            // Set scrollwheel option
            var map_options = { scrollwheel : opts.scrollwheel, disableDoubleClickZoom : !opts.doubleclickzoom };
            // Check for map controls
            if (opts.controls === false) {
                $.extend(map_options, { disableDefaultUI : true });
            } else if (opts.controls.length != 0) {
                $.extend(map_options, opts.controls, { disableDefaultUI : true });
            }

            $gmap.setOptions(map_options);

            // Create new icon
            var gicon = new google.maps.Marker();

            // Set icon properties from global options
            marker_icon = new google.maps.MarkerImage(opts.icon.image);
            marker_icon.size = new google.maps.Size(opts.icon.iconsize[0], opts.icon.iconsize[1]);
            marker_icon.anchor = new google.maps.Point(opts.icon.iconanchor[0], opts.icon.iconanchor[1]);
            gicon.setIcon(marker_icon);

            if (opts.icon.shadow) {
                marker_shadow = new google.maps.MarkerImage(opts.icon.shadow);
                marker_shadow.size = new google.maps.Size(opts.icon.shadowsize[0], opts.icon.shadowsize[1]);
                marker_shadow.anchor = new google.maps.Point(opts.icon.shadowanchor[0], opts.icon.shadowanchor[1]);
                gicon.setShadow(marker_shadow);
            }

            // Bind actions
            $(this).bind('gMap.centerAt', function (e, latitude, longitude, zoom) {
                if (zoom) {
                    $gmap.setZoom(zoom);
                }

                $gmap.panTo(new google.maps.LatLng(parseFloat(latitude), parseFloat(longitude)));
            });

            var last_infowindow;
            $(this).bind('gMap.addMarker', function (e, latitude, longitude, content, icon, popup) {
                var glatlng = new google.maps.LatLng(parseFloat(latitude), parseFloat(longitude));

                var gmarker = new google.maps.Marker({
                    position : glatlng
                });

                if (icon) {
                    marker_icon = new google.maps.MarkerImage(icon.image);
                    marker_icon.size = new google.maps.Size(icon.iconsize[0], icon.iconsize[1]);
                    marker_icon.anchor = new google.maps.Point(icon.iconanchor[0], icon.iconanchor[1]);
                    gmarker.setIcon(marker_icon);

                    if (icon.shadow) {
                        marker_shadow = new google.maps.MarkerImage(icon.shadow);
                        marker_shadow.size = new google.maps.Size(icon.shadowsize[0], icon.shadowsize[1]);
                        marker_shadow.anchor = new google.maps.Point(icon.shadowanchor[0], icon.shadowanchor[1]);
                        gicon.setShadow(marker_shadow);
                    }
                } else {
                    gmarker.setIcon(gicon.getIcon());
                    gmarker.setShadow(gicon.getShadow());
                }

                if (content) {
                    if (content == '_latlng') {
                        content = latitude + ', ' + longitude;
                    }

                    var infowindow = new google.maps.InfoWindow({
                        content : opts.html_prepend + content + opts.html_append
                    });

                    google.maps.event.addListener(gmarker, 'click', function () {
                        last_infowindow && last_infowindow.close();
                        infowindow.open($gmap, gmarker);
                        last_infowindow = infowindow;
                    });

                    if (popup) {
                        google.maps.event.addListenerOnce($gmap, 'tilesloaded', function () {
                            infowindow.open($gmap, gmarker);
                        });
                    }
                }
                gmarker.setMap($gmap);
            });

            // Loop through marker array
            for (var j = 0; j < opts.markers.length; j++) {
                // Get the options from current marker
                marker = opts.markers[j];

                // Check if address is available
                if (marker.address) {
                    // Check for reference to the marker's address
                    if (marker.html == '_address') {
                        marker.html = marker.address;
                    }

                    // Get the point for given address
                    var $this = this;
                    $geocoder.geocode({
                            address : marker.address
                        }, (function (marker, $this) {
                        return function (gresult, status) {
                            // Create marker
                            if (gresult && gresult.length > 0) {
                                $($this).trigger('gMap.addMarker', [gresult[0].geometry.location.lat(), gresult[0].geometry.location.lng(), marker.html, marker.icon, marker.popup]);
                            }
                        };
                    })(marker, $this));
                } else {
                    $(this).trigger('gMap.addMarker', [marker.latitude, marker.longitude, marker.html, marker.icon, marker.popup]);
                }
            }
        });

    }

    // Default settings
    $.fn.gMap.defaults = {
        address         : '',
        latitude        : 0,
        longitude       : 0,
        zoom            : 1,
        markers         : [],
        controls        : [],
        scrollwheel     : false,
        doubleclickzoom : true,
        maptype         : 'ROADMAP',
        html_prepend    : '<div class="gmap_marker">',
        html_append     : '</div>',
        icon            : {
            image        : "http://www.google.com/mapfiles/marker.png",
            shadow       : "http://www.google.com/mapfiles/shadow50.png",
            iconsize     : [20, 34],
            shadowsize   : [37, 34],
            iconanchor   : [9, 34],
            shadowanchor : [6, 34]
        }
    }

})(jQuery);
/**
* Note: While Microsoft is not the author of this file, Microsoft is
* offering you a license subject to the terms of the Microsoft Software
* License Terms for Microsoft ASP.NET Model View Controller 3.
* Microsoft reserves all other rights. The notices below are provided
* for informational purposes only and are not the license terms under
* which Microsoft distributed this file.
*
* jQuery Validation Plugin 1.8.0
*
* http://bassistance.de/jquery-plugins/jquery-plugin-validation/
* http://docs.jquery.com/Plugins/Validation
*
* Copyright (c) 2006 - 2011 Jörn Zaefferer
*/
(function(c){c.extend(c.fn,{validate:function(a){if(this.length){var b=c.data(this[0],"validator");if(b)return b;b=new c.validator(a,this[0]);c.data(this[0],"validator",b);if(b.settings.onsubmit){this.find("input, button").filter(".cancel").click(function(){b.cancelSubmit=true});b.settings.submitHandler&&this.find("input, button").filter(":submit").click(function(){b.submitButton=this});this.submit(function(d){function e(){if(b.settings.submitHandler){if(b.submitButton)var f=c("<input type='hidden'/>").attr("name",
b.submitButton.name).val(b.submitButton.value).appendTo(b.currentForm);b.settings.submitHandler.call(b,b.currentForm);b.submitButton&&f.remove();return false}return true}b.settings.debug&&d.preventDefault();if(b.cancelSubmit){b.cancelSubmit=false;return e()}if(b.form()){if(b.pendingRequest){b.formSubmitted=true;return false}return e()}else{b.focusInvalid();return false}})}return b}else a&&a.debug&&window.console&&console.warn("nothing selected, can't validate, returning nothing")},valid:function(){if(c(this[0]).is("form"))return this.validate().form();
else{var a=true,b=c(this[0].form).validate();this.each(function(){a&=b.element(this)});return a}},removeAttrs:function(a){var b={},d=this;c.each(a.split(/\s/),function(e,f){b[f]=d.attr(f);d.removeAttr(f)});return b},rules:function(a,b){var d=this[0];if(a){var e=c.data(d.form,"validator").settings,f=e.rules,g=c.validator.staticRules(d);switch(a){case "add":c.extend(g,c.validator.normalizeRule(b));f[d.name]=g;if(b.messages)e.messages[d.name]=c.extend(e.messages[d.name],b.messages);break;case "remove":if(!b){delete f[d.name];
return g}var h={};c.each(b.split(/\s/),function(j,i){h[i]=g[i];delete g[i]});return h}}d=c.validator.normalizeRules(c.extend({},c.validator.metadataRules(d),c.validator.classRules(d),c.validator.attributeRules(d),c.validator.staticRules(d)),d);if(d.required){e=d.required;delete d.required;d=c.extend({required:e},d)}return d}});c.extend(c.expr[":"],{blank:function(a){return!c.trim(""+a.value)},filled:function(a){return!!c.trim(""+a.value)},unchecked:function(a){return!a.checked}});c.validator=function(a,
b){this.settings=c.extend(true,{},c.validator.defaults,a);this.currentForm=b;this.init()};c.validator.format=function(a,b){if(arguments.length==1)return function(){var d=c.makeArray(arguments);d.unshift(a);return c.validator.format.apply(this,d)};if(arguments.length>2&&b.constructor!=Array)b=c.makeArray(arguments).slice(1);if(b.constructor!=Array)b=[b];c.each(b,function(d,e){a=a.replace(RegExp("\\{"+d+"\\}","g"),e)});return a};c.extend(c.validator,{defaults:{messages:{},groups:{},rules:{},errorClass:"error",
validClass:"valid",errorElement:"label",focusInvalid:true,errorContainer:c([]),errorLabelContainer:c([]),onsubmit:true,ignore:[],ignoreTitle:false,onfocusin:function(a){this.lastActive=a;if(this.settings.focusCleanup&&!this.blockFocusCleanup){this.settings.unhighlight&&this.settings.unhighlight.call(this,a,this.settings.errorClass,this.settings.validClass);this.addWrapper(this.errorsFor(a)).hide()}},onfocusout:function(a){if(!this.checkable(a)&&(a.name in this.submitted||!this.optional(a)))this.element(a)},
onkeyup:function(a){if(a.name in this.submitted||a==this.lastElement)this.element(a)},onclick:function(a){if(a.name in this.submitted)this.element(a);else a.parentNode.name in this.submitted&&this.element(a.parentNode)},highlight:function(a,b,d){c(a).addClass(b).removeClass(d)},unhighlight:function(a,b,d){c(a).removeClass(b).addClass(d)}},setDefaults:function(a){c.extend(c.validator.defaults,a)},messages:{required:"This field is required.",remote:"Please fix this field.",email:"Please enter a valid email address.",
url:"Please enter a valid URL.",date:"Please enter a valid date.",dateISO:"Please enter a valid date (ISO).",number:"Please enter a valid number.",digits:"Please enter only digits.",creditcard:"Please enter a valid credit card number.",equalTo:"Please enter the same value again.",accept:"Please enter a value with a valid extension.",maxlength:c.validator.format("Please enter no more than {0} characters."),minlength:c.validator.format("Please enter at least {0} characters."),rangelength:c.validator.format("Please enter a value between {0} and {1} characters long."),
range:c.validator.format("Please enter a value between {0} and {1}."),max:c.validator.format("Please enter a value less than or equal to {0}."),min:c.validator.format("Please enter a value greater than or equal to {0}.")},autoCreateRanges:false,prototype:{init:function(){function a(e){var f=c.data(this[0].form,"validator");e="on"+e.type.replace(/^validate/,"");f.settings[e]&&f.settings[e].call(f,this[0])}this.labelContainer=c(this.settings.errorLabelContainer);this.errorContext=this.labelContainer.length&&
this.labelContainer||c(this.currentForm);this.containers=c(this.settings.errorContainer).add(this.settings.errorLabelContainer);this.submitted={};this.valueCache={};this.pendingRequest=0;this.pending={};this.invalid={};this.reset();var b=this.groups={};c.each(this.settings.groups,function(e,f){c.each(f.split(/\s/),function(g,h){b[h]=e})});var d=this.settings.rules;c.each(d,function(e,f){d[e]=c.validator.normalizeRule(f)});c(this.currentForm).validateDelegate(":text, :password, :file, select, textarea",
"focusin focusout keyup",a).validateDelegate(":radio, :checkbox, select, option","click",a);this.settings.invalidHandler&&c(this.currentForm).bind("invalid-form.validate",this.settings.invalidHandler)},form:function(){this.checkForm();c.extend(this.submitted,this.errorMap);this.invalid=c.extend({},this.errorMap);this.valid()||c(this.currentForm).triggerHandler("invalid-form",[this]);this.showErrors();return this.valid()},checkForm:function(){this.prepareForm();for(var a=0,b=this.currentElements=this.elements();b[a];a++)this.check(b[a]);
return this.valid()},element:function(a){this.lastElement=a=this.clean(a);this.prepareElement(a);this.currentElements=c(a);var b=this.check(a);if(b)delete this.invalid[a.name];else this.invalid[a.name]=true;if(!this.numberOfInvalids())this.toHide=this.toHide.add(this.containers);this.showErrors();return b},showErrors:function(a){if(a){c.extend(this.errorMap,a);this.errorList=[];for(var b in a)this.errorList.push({message:a[b],element:this.findByName(b)[0]});this.successList=c.grep(this.successList,
function(d){return!(d.name in a)})}this.settings.showErrors?this.settings.showErrors.call(this,this.errorMap,this.errorList):this.defaultShowErrors()},resetForm:function(){c.fn.resetForm&&c(this.currentForm).resetForm();this.submitted={};this.prepareForm();this.hideErrors();this.elements().removeClass(this.settings.errorClass)},numberOfInvalids:function(){return this.objectLength(this.invalid)},objectLength:function(a){var b=0,d;for(d in a)b++;return b},hideErrors:function(){this.addWrapper(this.toHide).hide()},
valid:function(){return this.size()==0},size:function(){return this.errorList.length},focusInvalid:function(){if(this.settings.focusInvalid)try{c(this.findLastActive()||this.errorList.length&&this.errorList[0].element||[]).filter(":visible").focus().trigger("focusin")}catch(a){}},findLastActive:function(){var a=this.lastActive;return a&&c.grep(this.errorList,function(b){return b.element.name==a.name}).length==1&&a},elements:function(){var a=this,b={};return c([]).add(this.currentForm.elements).filter(":input").not(":submit, :reset, :image, [disabled]").not(this.settings.ignore).filter(function(){!this.name&&
a.settings.debug&&window.console&&console.error("%o has no name assigned",this);if(this.name in b||!a.objectLength(c(this).rules()))return false;return b[this.name]=true})},clean:function(a){return c(a)[0]},errors:function(){return c(this.settings.errorElement+"."+this.settings.errorClass,this.errorContext)},reset:function(){this.successList=[];this.errorList=[];this.errorMap={};this.toShow=c([]);this.toHide=c([]);this.currentElements=c([])},prepareForm:function(){this.reset();this.toHide=this.errors().add(this.containers)},
prepareElement:function(a){this.reset();this.toHide=this.errorsFor(a)},check:function(a){a=this.clean(a);if(this.checkable(a))a=this.findByName(a.name).not(this.settings.ignore)[0];var b=c(a).rules(),d=false,e;for(e in b){var f={method:e,parameters:b[e]};try{var g=c.validator.methods[e].call(this,a.value.replace(/\r/g,""),a,f.parameters);if(g=="dependency-mismatch")d=true;else{d=false;if(g=="pending"){this.toHide=this.toHide.not(this.errorsFor(a));return}if(!g){this.formatAndAdd(a,f);return false}}}catch(h){this.settings.debug&&
window.console&&console.log("exception occured when checking element "+a.id+", check the '"+f.method+"' method",h);throw h;}}if(!d){this.objectLength(b)&&this.successList.push(a);return true}},customMetaMessage:function(a,b){if(c.metadata){var d=this.settings.meta?c(a).metadata()[this.settings.meta]:c(a).metadata();return d&&d.messages&&d.messages[b]}},customMessage:function(a,b){var d=this.settings.messages[a];return d&&(d.constructor==String?d:d[b])},findDefined:function(){for(var a=0;a<arguments.length;a++)if(arguments[a]!==
undefined)return arguments[a]},defaultMessage:function(a,b){return this.findDefined(this.customMessage(a.name,b),this.customMetaMessage(a,b),!this.settings.ignoreTitle&&a.title||undefined,c.validator.messages[b],"<strong>Warning: No message defined for "+a.name+"</strong>")},formatAndAdd:function(a,b){var d=this.defaultMessage(a,b.method),e=/\$?\{(\d+)\}/g;if(typeof d=="function")d=d.call(this,b.parameters,a);else if(e.test(d))d=jQuery.format(d.replace(e,"{$1}"),b.parameters);this.errorList.push({message:d,
element:a});this.errorMap[a.name]=d;this.submitted[a.name]=d},addWrapper:function(a){if(this.settings.wrapper)a=a.add(a.parent(this.settings.wrapper));return a},defaultShowErrors:function(){for(var a=0;this.errorList[a];a++){var b=this.errorList[a];this.settings.highlight&&this.settings.highlight.call(this,b.element,this.settings.errorClass,this.settings.validClass);this.showLabel(b.element,b.message)}if(this.errorList.length)this.toShow=this.toShow.add(this.containers);if(this.settings.success)for(a=
0;this.successList[a];a++)this.showLabel(this.successList[a]);if(this.settings.unhighlight){a=0;for(b=this.validElements();b[a];a++)this.settings.unhighlight.call(this,b[a],this.settings.errorClass,this.settings.validClass)}this.toHide=this.toHide.not(this.toShow);this.hideErrors();this.addWrapper(this.toShow).show()},validElements:function(){return this.currentElements.not(this.invalidElements())},invalidElements:function(){return c(this.errorList).map(function(){return this.element})},showLabel:function(a,
b){var d=this.errorsFor(a);if(d.length){d.removeClass().addClass(this.settings.errorClass);d.attr("generated")&&d.html(b)}else{d=c("<"+this.settings.errorElement+"/>").attr({"for":this.idOrName(a),generated:true}).addClass(this.settings.errorClass).html(b||"");if(this.settings.wrapper)d=d.hide().show().wrap("<"+this.settings.wrapper+"/>").parent();this.labelContainer.append(d).length||(this.settings.errorPlacement?this.settings.errorPlacement(d,c(a)):d.insertAfter(a))}if(!b&&this.settings.success){d.text("");
typeof this.settings.success=="string"?d.addClass(this.settings.success):this.settings.success(d)}this.toShow=this.toShow.add(d)},errorsFor:function(a){var b=this.idOrName(a);return this.errors().filter(function(){return c(this).attr("for")==b})},idOrName:function(a){return this.groups[a.name]||(this.checkable(a)?a.name:a.id||a.name)},checkable:function(a){return/radio|checkbox/i.test(a.type)},findByName:function(a){var b=this.currentForm;return c(document.getElementsByName(a)).map(function(d,e){return e.form==
b&&e.name==a&&e||null})},getLength:function(a,b){switch(b.nodeName.toLowerCase()){case "select":return c("option:selected",b).length;case "input":if(this.checkable(b))return this.findByName(b.name).filter(":checked").length}return a.length},depend:function(a,b){return this.dependTypes[typeof a]?this.dependTypes[typeof a](a,b):true},dependTypes:{"boolean":function(a){return a},string:function(a,b){return!!c(a,b.form).length},"function":function(a,b){return a(b)}},optional:function(a){return!c.validator.methods.required.call(this,
c.trim(a.value),a)&&"dependency-mismatch"},startRequest:function(a){if(!this.pending[a.name]){this.pendingRequest++;this.pending[a.name]=true}},stopRequest:function(a,b){this.pendingRequest--;if(this.pendingRequest<0)this.pendingRequest=0;delete this.pending[a.name];if(b&&this.pendingRequest==0&&this.formSubmitted&&this.form()){c(this.currentForm).submit();this.formSubmitted=false}else if(!b&&this.pendingRequest==0&&this.formSubmitted){c(this.currentForm).triggerHandler("invalid-form",[this]);this.formSubmitted=
false}},previousValue:function(a){return c.data(a,"previousValue")||c.data(a,"previousValue",{old:null,valid:true,message:this.defaultMessage(a,"remote")})}},classRuleSettings:{required:{required:true},email:{email:true},url:{url:true},date:{date:true},dateISO:{dateISO:true},dateDE:{dateDE:true},number:{number:true},numberDE:{numberDE:true},digits:{digits:true},creditcard:{creditcard:true}},addClassRules:function(a,b){a.constructor==String?this.classRuleSettings[a]=b:c.extend(this.classRuleSettings,
a)},classRules:function(a){var b={};(a=c(a).attr("class"))&&c.each(a.split(" "),function(){this in c.validator.classRuleSettings&&c.extend(b,c.validator.classRuleSettings[this])});return b},attributeRules:function(a){var b={};a=c(a);for(var d in c.validator.methods){var e=a.attr(d);if(e)b[d]=e}b.maxlength&&/-1|2147483647|524288/.test(b.maxlength)&&delete b.maxlength;return b},metadataRules:function(a){if(!c.metadata)return{};var b=c.data(a.form,"validator").settings.meta;return b?c(a).metadata()[b]:
c(a).metadata()},staticRules:function(a){var b={},d=c.data(a.form,"validator");if(d.settings.rules)b=c.validator.normalizeRule(d.settings.rules[a.name])||{};return b},normalizeRules:function(a,b){c.each(a,function(d,e){if(e===false)delete a[d];else if(e.param||e.depends){var f=true;switch(typeof e.depends){case "string":f=!!c(e.depends,b.form).length;break;case "function":f=e.depends.call(b,b)}if(f)a[d]=e.param!==undefined?e.param:true;else delete a[d]}});c.each(a,function(d,e){a[d]=c.isFunction(e)?
e(b):e});c.each(["minlength","maxlength","min","max"],function(){if(a[this])a[this]=Number(a[this])});c.each(["rangelength","range"],function(){if(a[this])a[this]=[Number(a[this][0]),Number(a[this][1])]});if(c.validator.autoCreateRanges){if(a.min&&a.max){a.range=[a.min,a.max];delete a.min;delete a.max}if(a.minlength&&a.maxlength){a.rangelength=[a.minlength,a.maxlength];delete a.minlength;delete a.maxlength}}a.messages&&delete a.messages;return a},normalizeRule:function(a){if(typeof a=="string"){var b=
{};c.each(a.split(/\s/),function(){b[this]=true});a=b}return a},addMethod:function(a,b,d){c.validator.methods[a]=b;c.validator.messages[a]=d!=undefined?d:c.validator.messages[a];b.length<3&&c.validator.addClassRules(a,c.validator.normalizeRule(a))},methods:{required:function(a,b,d){if(!this.depend(d,b))return"dependency-mismatch";switch(b.nodeName.toLowerCase()){case "select":return(a=c(b).val())&&a.length>0;case "input":if(this.checkable(b))return this.getLength(a,b)>0;default:return c.trim(a).length>
0}},remote:function(a,b,d){if(this.optional(b))return"dependency-mismatch";var e=this.previousValue(b);this.settings.messages[b.name]||(this.settings.messages[b.name]={});e.originalMessage=this.settings.messages[b.name].remote;this.settings.messages[b.name].remote=e.message;d=typeof d=="string"&&{url:d}||d;if(this.pending[b.name])return"pending";if(e.old===a)return e.valid;e.old=a;var f=this;this.startRequest(b);var g={};g[b.name]=a;c.ajax(c.extend(true,{url:d,mode:"abort",port:"validate"+b.name,
dataType:"json",data:g,success:function(h){f.settings.messages[b.name].remote=e.originalMessage;var j=h===true;if(j){var i=f.formSubmitted;f.prepareElement(b);f.formSubmitted=i;f.successList.push(b);f.showErrors()}else{i={};h=h||f.defaultMessage(b,"remote");i[b.name]=e.message=c.isFunction(h)?h(a):h;f.showErrors(i)}e.valid=j;f.stopRequest(b,j)}},d));return"pending"},minlength:function(a,b,d){return this.optional(b)||this.getLength(c.trim(a),b)>=d},maxlength:function(a,b,d){return this.optional(b)||
this.getLength(c.trim(a),b)<=d},rangelength:function(a,b,d){a=this.getLength(c.trim(a),b);return this.optional(b)||a>=d[0]&&a<=d[1]},min:function(a,b,d){return this.optional(b)||a>=d},max:function(a,b,d){return this.optional(b)||a<=d},range:function(a,b,d){return this.optional(b)||a>=d[0]&&a<=d[1]},email:function(a,b){return this.optional(b)||/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(a)},
url:function(a,b){return this.optional(b)||/^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(a)},
date:function(a,b){return this.optional(b)||!/Invalid|NaN/.test(new Date(a))},dateISO:function(a,b){return this.optional(b)||/^\d{4}[\/-]\d{1,2}[\/-]\d{1,2}$/.test(a)},number:function(a,b){return this.optional(b)||/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(a)},digits:function(a,b){return this.optional(b)||/^\d+$/.test(a)},creditcard:function(a,b){if(this.optional(b))return"dependency-mismatch";if(/[^0-9-]+/.test(a))return false;var d=0,e=0,f=false;a=a.replace(/\D/g,"");for(var g=a.length-1;g>=
0;g--){e=a.charAt(g);e=parseInt(e,10);if(f)if((e*=2)>9)e-=9;d+=e;f=!f}return d%10==0},accept:function(a,b,d){d=typeof d=="string"?d.replace(/,/g,"|"):"png|jpe?g|gif";return this.optional(b)||a.match(RegExp(".("+d+")$","i"))},equalTo:function(a,b,d){d=c(d).unbind(".validate-equalTo").bind("blur.validate-equalTo",function(){c(b).valid()});return a==d.val()}}});c.format=c.validator.format})(jQuery);
(function(c){var a={};if(c.ajaxPrefilter)c.ajaxPrefilter(function(d,e,f){e=d.port;if(d.mode=="abort"){a[e]&&a[e].abort();a[e]=f}});else{var b=c.ajax;c.ajax=function(d){var e=("port"in d?d:c.ajaxSettings).port;if(("mode"in d?d:c.ajaxSettings).mode=="abort"){a[e]&&a[e].abort();return a[e]=b.apply(this,arguments)}return b.apply(this,arguments)}}})(jQuery);
(function(c){!jQuery.event.special.focusin&&!jQuery.event.special.focusout&&document.addEventListener&&c.each({focus:"focusin",blur:"focusout"},function(a,b){function d(e){e=c.event.fix(e);e.type=b;return c.event.handle.call(this,e)}c.event.special[b]={setup:function(){this.addEventListener(a,d,true)},teardown:function(){this.removeEventListener(a,d,true)},handler:function(e){arguments[0]=c.event.fix(e);arguments[0].type=b;return c.event.handle.apply(this,arguments)}}});c.extend(c.fn,{validateDelegate:function(a,
b,d){return this.bind(b,function(e){var f=c(e.target);if(f.is(a))return d.apply(f,arguments)})}})})(jQuery);

/*
** Unobtrusive validation support library for jQuery and jQuery Validate
** Copyright (C) Microsoft Corporation. All rights reserved.
*/
(function(a){var d=a.validator,b,f="unobtrusiveValidation";function c(a,b,c){a.rules[b]=c;if(a.message)a.messages[b]=a.message}function i(a){return a.replace(/^\s+|\s+$/g,"").split(/\s*,\s*/g)}function g(a){return a.substr(0,a.lastIndexOf(".")+1)}function e(a,b){if(a.indexOf("*.")===0)a=a.replace("*.",b);return a}function l(c,d){var b=a(this).find("[data-valmsg-for='"+d[0].name+"']"),e=a.parseJSON(b.attr("data-valmsg-replace"))!==false;b.removeClass("field-validation-valid").addClass("field-validation-error");c.data("unobtrusiveContainer",b);if(e){b.empty();c.removeClass("input-validation-error").appendTo(b)}else c.hide()}function k(e,d){var c=a(this).find("[data-valmsg-summary=true]"),b=c.find("ul");if(b&&b.length&&d.errorList.length){b.empty();c.addClass("validation-summary-errors").removeClass("validation-summary-valid");a.each(d.errorList,function(){a("<li />").html(this.message).appendTo(b)})}}function j(c){var b=c.data("unobtrusiveContainer"),d=a.parseJSON(b.attr("data-valmsg-replace"));if(b){b.addClass("field-validation-valid").removeClass("field-validation-error");c.removeData("unobtrusiveContainer");d&&b.empty()}}function h(d){var b=a(d),c=b.data(f);if(!c){c={options:{errorClass:"input-validation-error",errorElement:"span",errorPlacement:a.proxy(l,d),invalidHandler:a.proxy(k,d),messages:{},rules:{},success:a.proxy(j,d)},attachValidation:function(){b.validate(this.options)},validate:function(){b.validate();return b.valid()}};b.data(f,c)}return c}d.unobtrusive={adapters:[],parseElement:function(b,i){var d=a(b),f=d.parents("form")[0],c,e,g;if(!f)return;c=h(f);c.options.rules[b.name]=e={};c.options.messages[b.name]=g={};a.each(this.adapters,function(){var c="data-val-"+this.name,i=d.attr(c),h={};if(i!==undefined){c+="-";a.each(this.params,function(){h[this]=d.attr(c+this)});this.adapt({element:b,form:f,message:i,params:h,rules:e,messages:g})}});jQuery.extend(e,{__dummy__:true});!i&&c.attachValidation()},parse:function(b){a(b).find(":input[data-val=true]").each(function(){d.unobtrusive.parseElement(this,true)});a("form").each(function(){var a=h(this);a&&a.attachValidation()})}};b=d.unobtrusive.adapters;b.add=function(c,a,b){if(!b){b=a;a=[]}this.push({name:c,params:a,adapt:b});return this};b.addBool=function(a,b){return this.add(a,function(d){c(d,b||a,true)})};b.addMinMax=function(e,g,f,a,d,b){return this.add(e,[d||"min",b||"max"],function(b){var e=b.params.min,d=b.params.max;if(e&&d)c(b,a,[e,d]);else if(e)c(b,g,e);else d&&c(b,f,d)})};b.addSingleVal=function(a,b,d){return this.add(a,[b||"val"],function(e){c(e,d||a,e.params[b])})};d.addMethod("__dummy__",function(){return true});d.addMethod("regex",function(b,c,d){var a;if(this.optional(c))return true;a=(new RegExp(d)).exec(b);return a&&a.index===0&&a[0].length===b.length});b.addSingleVal("accept","exts").addSingleVal("regex","pattern");b.addBool("creditcard").addBool("date").addBool("digits").addBool("email").addBool("number").addBool("url");b.addMinMax("length","minlength","maxlength","rangelength").addMinMax("range","min","max","range");b.add("equalto",["other"],function(b){var h=g(b.element.name),i=b.params.other,d=e(i,h),f=a(b.form).find(":input[name="+d+"]")[0];c(b,"equalTo",f)});b.add("required",function(a){(a.element.tagName.toUpperCase()!=="INPUT"||a.element.type.toUpperCase()!=="CHECKBOX")&&c(a,"required",true)});b.add("remote",["url","type","additionalfields"],function(b){var d={url:b.params.url,type:b.params.type||"GET",data:{}},f=g(b.element.name);a.each(i(b.params.additionalfields||b.element.name),function(h,g){var c=e(g,f);d.data[c]=function(){return a(b.form).find(":input[name='"+c+"']").val()}});c(b,"remote",d)});a(function(){d.unobtrusive.parse(document)})})(jQuery);
/**
 * Hide address bar on mobile devices (except if #hash present, so we don't mess up deep linking).
 */
if (Modernizr.touch && !window.location.hash) {
    $(window).load(function () {
        setTimeout(function () {
            window.scrollTo(0, 1);
        }, 0);
    });
}
/**
 * Placeholder backup
 */
if (!Modernizr.input.placeholder) {
    var $placeholder = $('[placeholder]');

    $placeholder.focus(function () {
        var input = $(this);
        if (input.val() === input.attr('placeholder')) {
            input.val('');
        }
    }).blur(function () {
            var input = $(this);
            if (input.val() === '' || input.val() === input.attr('placeholder')) {
                input.val(input.attr('placeholder'));
            }
        }).blur();

    $placeholder.parents('form').submit(function () {
        $(this).find('[placeholder]').each(function () {
            var input = $(this);
            if (input.val() === input.attr('placeholder')) {
                input.val('');
            }
        });
    });
}
/**
 * Set default jquery datepicker format
 */
if ($.datepicker) {
    $.datepicker.setDefaults($.datepicker.regional['en-GB']);
    $.datepicker.setDefaults({ dateFormat : 'dd/mm/yy' });

    /* jQuery validation fix for datepicker UK format */
    if (!Modernizr.inputtypes.date) {
        $(function () {
            $.datepicker.setDefaults({ dateFormat : 'dd/mm/yy' });
            $('.datefield').datepicker();
        });

        jQuery.validator.addMethod(
            'date',
            function (value, element, params) {
                if (this.optional(element)) {
                    return true;
                }
                var result = false;
                try {
                    $.datepicker.parseDate('dd/mm/yy', value);
                    result = true;
                } catch (err) {
                    result = false;
                }
                return result;
            },
            ''
        );
    }
}

$.fn.serialize = function (options) {
    return $.param(this.serializeArray(options));
};

$.fn.serializeArray = function (options) {
    var o = $.extend({
        checkboxesAsBools : false
    }, options || {});

    var rselectTextarea = /select|textarea/i;
    var rinput = /text|hidden|password|search/i;

    return this.map(function () {
        return this.elements ? $.makeArray(this.elements) : this;
    })
        .filter(function () {
            return this.name && !this.disabled &&
                (this.checked
                    || (o.checkboxesAsBools && this.type === 'checkbox')
                    || rselectTextarea.test(this.nodeName)
                    || rinput.test(this.type));
        })
        .map(function (i, elem) {
            var val = $(this).val();
            return val == null ?
                   null :
                   $.isArray(val) ?
                   $.map(val, function (val, i) {
                       return { name : elem.name, value : val };
                   }) :
                   {
                       name : elem.name,
                       value : (o.checkboxesAsBools && this.type === 'checkbox') ? //moar ternaries!
                               (this.checked ? 'true' : 'false') :
                               val
                   };
        }).get();
};

/**
 * Polyfills
 *
 * @url         --
 * @author      Vladimir Bujanovic
 * @version     0.1
 *
 */

/**
 * Object.create for ECMAScript 5 Support
 */
if (typeof Object.create !== 'function') {
    Object.create = function (o, props) {
        function F() {}

        F.prototype = o;

        if (typeof(props) === "object") {
            for (prop in props) {
                if (props.hasOwnProperty((prop))) {
                    F[prop] = props[prop];
                    console.log(F[prop]);
                }
            }
        }

        return new F();
    };
}
/*--------------------------------------
 * App Core
 ---------------------------------------*/

;
(function ($) {
    window.App = {
        Core: {},
        Components: {},
        Pages: {},
        Constants: {
            loadingGif: '',
            Prefix: (function () {
                var styles = window.getComputedStyle(document.documentElement, ''),
                    pre = (Array.prototype.slice
                        .call(styles)
                        .join('')
                        .match(/-(moz|webkit|ms)-/) || (styles.OLink === '' && ['', 'o'])
                        )[1],
                    dom = ('WebKit|Moz|MS|O').match(new RegExp('(' + pre + ')', 'i'))[1];
                return {
                    dom: dom,
                    lowercase: pre,
                    css: '-' + pre + '-',
                    js: pre[0].toUpperCase() + pre.substr(1)
                };
            })()
        },
        Helpers: {
            log: function () {
                window.console && console.log('App log:', Array.prototype.slice.call(arguments));
            }
        },
        Instances: {}
    };

    /**
     * Log application object
     */

    App.Helpers.log(App);

})(jQuery);
/*--------------------------------------
 * App View
 ---------------------------------------*/

;
(function (App, $) {
    var Core = App.Core,
        Helpers = App.Helpers;

    Core.Component = {
        wrapper : '#component',

        initialize : function () {
            var base = this;

            base.setElement();

            if(!base.$wrapper.length > 0) {
                return false;
            }

            base.render();
            base.bindEvents();

            return base;
        },

        render : function(){},

        bindEvents : function(){},

        destroy : function(){
            this.$wrapper.remove();
            delete this;
        },

        extend : function (props) {
            var prop, obj;
            obj = Object.create(this);
            for (prop in props) {
                if (props.hasOwnProperty(prop)) {
                    obj[prop] = props[prop];
                }
            }

            return obj;
        },

        setElement : function () {

            if (this.wrapper) {
                this.$wrapper = this.wrapper instanceof jQuery ? this.wrapper : $(this.wrapper);
            }
        }
    };

})(App || {}, jQuery);
/*--------------------------------------
 * App View
 ---------------------------------------*/

;
(function (App, $) {
    var Core = App.Core,
        Helpers = App.Helpers;

    Core.Page = {
        wrapper : '#page-content',

        extend : function (props) {
            var prop, obj;
            obj = Object.create(this);
            for (prop in props) {
                if (props.hasOwnProperty(prop)) {
                    obj[prop] = props[prop];
                }
            }

            return obj;
        },

        setElement : function () {

            if (this.wrapper) {
                this.$wrapper = this.wrapper instanceof jQuery ? this.wrapper : $(this.wrapper);
            }
        }
    };

})(App || {}, jQuery);
/**
 * Home page View
 *
 * @url         --
 * @author      Vladimir Bujanovic
 * @version     0.2
 *
 * @require core/App.js
 * @require core/App.View.js
 * @require modules/*.js
 */

;
(function (App, $) {
    var Core = App.Core,
        Page = Core.Page,
        Components = App.Components,
        Instances = App.Instances,
        Helpers = App.Helpers;

    App.Pages.Home = Page.extend({

        initialize: function () {

            Instances.HomeAccordion = Components.Accordion.extend({
                wrapper : '#accordion'
            }).initialize();

            Instances.Modal = Object.create(Components.Modal).initialize();

            Instances.MainTabs = Components.Tabs.extend({
                wrapper: '#tabs'
            }).initialize();

        }

    });

})(App || {}, jQuery);
/**
 * Responsive Accordion component
 *
 * @url         --
 * @author      Vladimir Bujanovic
 * @version     0.4
 *
 * @require core/App.js
 * @require core/App.Component.js
 */

;
(function (App, $, undefined) {
    var Component = App.Core.Component,
        Helpers = App.Helpers;

    App.Components.Accordion = Component.extend({
        wrapper : '.accordion',
        trigger : 'dt',
        control : 'dd',
        speed : 400,

        bindEvents : function () {
            this.$wrapper.on('click', this.trigger, $.proxy(this.animateContent, this));
        },

        animateContent : function (e) {

            var $this = $(e.currentTarget);

            this.$wrapper.find(this.trigger).removeClass('active');
            this.$wrapper.find(this.control).slideUp(this.speed);

            $this.addClass('active').next().slideDown(this.speed);
        }
    });

})(App || {}, jQuery, undefined);


/**
 * Responsive Modal component
 *
 * @url         --
 * @author      Vladimir Bujanovic
 * @version     0.4
 *
 * @require core/App.js
 * @require core/App.Component.js
 */

;
(function (App, $, undefined) {
    var Component = App.Core.Component,
        Helpers = App.Helpers;

    App.Components.Modal = Component.extend({
        wrapper: '#modal',
        trigger: '.modal-btn',
        parent: '#page-content',
        maxWidth: 800,
        minWidth: 400,
        speed: 400,
        onShow: $.noop,
        onClose: $.noop,
        getData: $.noop,
        submitForm: $.noop,

        render: function () {
            var htmlClose = '<span class="modal-close"></span>';

            this.$inner = $('<div class="modal-inner-wrapper"></div>');
            this.$mask = $('<div class="modal-mask"></div>');

            this.$wrapper
                .css({
                    minWidth: this.minWidth,
                    maxWidth: this.maxWidth
                })
                .append(htmlClose)
                .append(this.$inner)
                .append(this.$mask);

        },

        bindEvents: function () {
            var base = this;

            base.$wrapper
                .on('click', ':reset', $.proxy(base.close, base))

                .on('click', ':submit', $.proxy(base.submitForm, base))

                .on('click', '.modal-close', $.proxy(base.close, base))

                .on('click', '.modal-mask', $.proxy(base.close, base));

            base.$parent
                .on('click', this.trigger, function (e) {
                    e.preventDefault();
                    var url = $(e.currentTarget).attr('href');

                    $.ajax({
                        type: "GET",
                        url: url,
                        cache: false,
                        async: false,
                        success: function (response) {
                            base.open(response);
                        },
                        error: function (error) {
                            console.error(error);
                        }
                    });
                });

            $(window).resize(function () {
                base.centerDialog();
            });

            $(document).keyup(function (e) {
                // esc key pressed
                if (e.keyCode == 27) {
                    base.close();
                }
            });
        },

        centerDialog: function () {
            var base = this,
                ww = $(window).width(),
                wh = $(window).height();

            base.$wrapper.css({
                left: (ww - base.$wrapper.width()) / 2,
                top: (wh - base.$wrapper.height()) / 2 + $(window).scrollTop()
            });

            base.$mask.css({
                width: ww,
                height: wh
            });
        },

        open: function (html) {
            var base = this;

            base.$inner.html(html);
            base.$mask.show();
            base.centerDialog();
            base.$wrapper.fadeIn(base.speed, function () {
                base.onShow.call(base);
            });

            return base;
        },

        close: function (data) {
            var base = this;

            base.$wrapper.fadeOut(base.speed, function () {
                base.$inner.html('');
                base.$mask.hide();
                base.onClose.call(base, data);
            });

            return base;
        },

        setElement: function () {

            if (this.wrapper) {
                this.$wrapper = this.wrapper instanceof jQuery ? this.wrapper : $(this.wrapper);
            }
            if (this.parent) {
                this.$parent = this.parent instanceof jQuery ? this.parent : $(this.parent);
            }
        }

    });

})(App || {}, jQuery, undefined);


/**
 * Responsive Navigation component
 *
 * @url         --
 * @author      Vladimir Bujanovic
 * @version     0.4
 *
 * @require core/App.js
 * @require core/App.Component.js
 */

;
(function (App, $, undefined) {
    var Component = App.Core.Component,
        Helpers = App.Helpers,
        Constants = App.Constants;

    App.Components.ResponsiveNav = Component.extend({
        wrapper: '#res-nav',
        trigger: '#pull-btn',
        width: 767,
        onOpen: $.noop,
        onClose: $.noop,

        render: function () {
            var base = this;

            base.$list = this.$wrapper.find('ul');

            if (Modernizr.csstransitions) {
                base.$list.css({
                    'max-height': 0,
                    'display': 'block'
                });
            }

            return base;
        },

        bindEvents: function () {
            var base = this;

            base.$wrapper.on('touchstart click', base.trigger, $.proxy(base.toggle, this));

            $(window).resize(function () {
                var w = $(window).width();

                if (w > base.width && base.$list.is(':hidden')) {
                    base.$list.removeAttr('style');
                }
            });
        },

        toggle: function (e) {
            var base = this;

            e.preventDefault();

            if (base.$wrapper.hasClass('expanded')) {
                base.close();
            } else {
                base.open();
            }
        },

        open: function () {
            var base = this;

            if (Modernizr.csstransitions) {

                base.$list
                    .css('max-height', 1000)
                    .css(Constants.Prefix.js + 'Transition', "max-height 1.0s ease-in-out");

            } else {
                base.$list.slideDown();
            }

            base.$wrapper.addClass('expanded');
            //Callback
            base.onOpen.call(base);

            return base;
        },

        close: function () {
            var base = this;

            if (Modernizr.csstransitions) {

                base.$list
                    .css('max-height', 0)
                    .css(Constants.Prefix.js + 'Transition', "max-height 1.0s ease-in-out");

            } else {
                base.$list.slideUp();
            }

            base.$wrapper.removeClass('expanded');
            //Callback
            base.onClose.call(base);

            return base;
        }

    });

})(App || {}, jQuery, undefined);


/**
 * Responsive Tabs component
 *
 * @url         --
 * @author      Vladimir Bujanovic
 * @version     0.4
 *
 * @require core/App.js
 * @require core/App.Component.js
 */
;
(function (App, $, undefined) {
    var Component = App.Core.Component,
        Helpers = App.Helpers;

    App.Components.Tabs = Component.extend({
        wrapper: '#tabs',
        nav: 'li',
        content: '.tab',
        activeCssClass: 'active',
        animated: true,
        onBeforeChange: $.noop,
        onAfterChange: $.noop,

        render: function () {
            if (this.animated) {
                this.$content.addClass('animated');
            }

            var tabName = window.location.hash.slice(1).length > 0 ?
                          window.location.hash.slice(1) :
                          this.$content.eq(0).attr('data-tab');

            this.getTabNames();
            this.changeTab(tabName);
        },

        getTabNames: function () {
            var base = this;

            this.tabs = [];
            this.$nav.each(function () {
                base.tabs.push($(this).data('tab'));
            });
        },


        bindEvents: function () {
            var base = this;

            this.$wrapper.on('click', base.nav, function (e) {
                e.preventDefault();

                base.changeTab($(this).data('tab'));
            });

        },

        changeTab: function (tabName) {
            //On before change callback
            this.onBeforeChange.call(this, tabName);

            this.$nav.removeClass(this.activeCssClass);
            this.$content.removeClass(this.activeCssClass);

            if (tabName && $.inArray(tabName, this.tabs) > -1) {
                this.$nav.filter('[data-tab="' + tabName + '"]').addClass(this.activeCssClass);
                this.$content.filter('[data-tab="' + tabName + '"]').addClass(this.activeCssClass);
            }
            else {
                this.$nav.eq(0).addClass(this.activeCssClass);
                this.$content.eq(0).addClass(this.activeCssClass);
            }

            window.location.hash = tabName;

            //On after change callback
            this.onAfterChange.call(this, tabName);
        },

        setElement: function () {
            if (this.wrapper) {
                this.$wrapper = this.wrapper instanceof jQuery ? this.wrapper : $(this.wrapper);
                this.$nav = this.$wrapper.find(this.nav);
                this.$content = this.$wrapper.find(this.content);
            }
        }

    });

})(App || {}, jQuery, undefined);


/**
 * Master page View
 *
 * @url         --
 * @author      Vladimir Bujanovic
 * @version     0.2
 *
 * @require core/App.js
 * @require core/App.View.js
 * @require modules/*.js
 */

;
(function (App, $) {
    var Core = App.Core,
        Pages = App.Pages,
        Components = App.Components,
        Helpers = App.Helpers,
        Instances = App.Instances;

    var pageName = $('#page-content').data('page');

    /**
     * Create page instance
     */

    if (pageName !== '') {

        Instances[pageName] = Pages[pageName].extend().initialize();
    }

    /**
     * Create instance of navigation component
     */
    Instances.MainNav = Components.ResponsiveNav.extend({
        wrapper : '#main-nav'

    }).initialize();

})(App || {}, jQuery);