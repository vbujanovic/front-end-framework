/*--------------------------------------
 * App Core
 ---------------------------------------*/

;
(function ($) {
    window.App = {
        Core: {},
        Components: {},
        Pages: {},
        Constants: {
            loadingGif: '',
            Prefix: (function () {
                var styles = window.getComputedStyle(document.documentElement, ''),
                    pre = (Array.prototype.slice
                        .call(styles)
                        .join('')
                        .match(/-(moz|webkit|ms)-/) || (styles.OLink === '' && ['', 'o'])
                        )[1],
                    dom = ('WebKit|Moz|MS|O').match(new RegExp('(' + pre + ')', 'i'))[1];
                return {
                    dom: dom,
                    lowercase: pre,
                    css: '-' + pre + '-',
                    js: pre[0].toUpperCase() + pre.substr(1)
                };
            })()
        },
        Helpers: {
            log: function () {
                window.console && console.log('App log:', Array.prototype.slice.call(arguments));
            }
        },
        Instances: {}
    };

    /**
     * Log application object
     */

    App.Helpers.log(App);

})(jQuery);