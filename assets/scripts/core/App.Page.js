/*--------------------------------------
 * App View
 ---------------------------------------*/

;
(function (App, $) {
    var Core = App.Core,
        Helpers = App.Helpers;

    Core.Page = {
        wrapper : '#page-content',

        extend : function (props) {
            var prop, obj;
            obj = Object.create(this);
            for (prop in props) {
                if (props.hasOwnProperty(prop)) {
                    obj[prop] = props[prop];
                }
            }

            return obj;
        },

        setElement : function () {

            if (this.wrapper) {
                this.$wrapper = this.wrapper instanceof jQuery ? this.wrapper : $(this.wrapper);
            }
        }
    };

})(App || {}, jQuery);