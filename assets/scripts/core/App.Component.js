/*--------------------------------------
 * App View
 ---------------------------------------*/

;
(function (App, $) {
    var Core = App.Core,
        Helpers = App.Helpers;

    Core.Component = {
        wrapper : '#component',

        initialize : function () {
            var base = this;

            base.setElement();

            if(!base.$wrapper.length > 0) {
                return false;
            }

            base.render();
            base.bindEvents();

            return base;
        },

        render : function(){},

        bindEvents : function(){},

        destroy : function(){
            this.$wrapper.remove();
            delete this;
        },

        extend : function (props) {
            var prop, obj;
            obj = Object.create(this);
            for (prop in props) {
                if (props.hasOwnProperty(prop)) {
                    obj[prop] = props[prop];
                }
            }

            return obj;
        },

        setElement : function () {

            if (this.wrapper) {
                this.$wrapper = this.wrapper instanceof jQuery ? this.wrapper : $(this.wrapper);
            }
        }
    };

})(App || {}, jQuery);