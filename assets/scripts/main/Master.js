/**
 * Master page View
 *
 * @url         --
 * @author      Vladimir Bujanovic
 * @version     0.2
 *
 * @require core/App.js
 * @require core/App.View.js
 * @require modules/*.js
 */

;
(function (App, $) {
    var Core = App.Core,
        Pages = App.Pages,
        Components = App.Components,
        Helpers = App.Helpers,
        Instances = App.Instances;

    var pageName = $('#page-content').data('page');

    /**
     * Create page instance
     */

    if (pageName !== '') {

        Instances[pageName] = Pages[pageName].extend().initialize();
    }

    /**
     * Create instance of navigation component
     */
    Instances.MainNav = Components.ResponsiveNav.extend({
        wrapper : '#main-nav'

    }).initialize();

})(App || {}, jQuery);