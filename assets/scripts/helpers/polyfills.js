/**
 * Polyfills
 *
 * @url         --
 * @author      Vladimir Bujanovic
 * @version     0.1
 *
 */

/**
 * Object.create for ECMAScript 5 Support
 */
if (typeof Object.create !== 'function') {
    Object.create = function (o, props) {
        function F() {}

        F.prototype = o;

        if (typeof(props) === "object") {
            for (prop in props) {
                if (props.hasOwnProperty((prop))) {
                    F[prop] = props[prop];
                    console.log(F[prop]);
                }
            }
        }

        return new F();
    };
}