/**
 * Hide address bar on mobile devices (except if #hash present, so we don't mess up deep linking).
 */
if (Modernizr.touch && !window.location.hash) {
    $(window).load(function () {
        setTimeout(function () {
            window.scrollTo(0, 1);
        }, 0);
    });
}
/**
 * Placeholder backup
 */
if (!Modernizr.input.placeholder) {
    var $placeholder = $('[placeholder]');

    $placeholder.focus(function () {
        var input = $(this);
        if (input.val() === input.attr('placeholder')) {
            input.val('');
        }
    }).blur(function () {
            var input = $(this);
            if (input.val() === '' || input.val() === input.attr('placeholder')) {
                input.val(input.attr('placeholder'));
            }
        }).blur();

    $placeholder.parents('form').submit(function () {
        $(this).find('[placeholder]').each(function () {
            var input = $(this);
            if (input.val() === input.attr('placeholder')) {
                input.val('');
            }
        });
    });
}
/**
 * Set default jquery datepicker format
 */
if ($.datepicker) {
    $.datepicker.setDefaults($.datepicker.regional['en-GB']);
    $.datepicker.setDefaults({ dateFormat : 'dd/mm/yy' });

    /* jQuery validation fix for datepicker UK format */
    if (!Modernizr.inputtypes.date) {
        $(function () {
            $.datepicker.setDefaults({ dateFormat : 'dd/mm/yy' });
            $('.datefield').datepicker();
        });

        jQuery.validator.addMethod(
            'date',
            function (value, element, params) {
                if (this.optional(element)) {
                    return true;
                }
                var result = false;
                try {
                    $.datepicker.parseDate('dd/mm/yy', value);
                    result = true;
                } catch (err) {
                    result = false;
                }
                return result;
            },
            ''
        );
    }
}

$.fn.serialize = function (options) {
    return $.param(this.serializeArray(options));
};

$.fn.serializeArray = function (options) {
    var o = $.extend({
        checkboxesAsBools : false
    }, options || {});

    var rselectTextarea = /select|textarea/i;
    var rinput = /text|hidden|password|search/i;

    return this.map(function () {
        return this.elements ? $.makeArray(this.elements) : this;
    })
        .filter(function () {
            return this.name && !this.disabled &&
                (this.checked
                    || (o.checkboxesAsBools && this.type === 'checkbox')
                    || rselectTextarea.test(this.nodeName)
                    || rinput.test(this.type));
        })
        .map(function (i, elem) {
            var val = $(this).val();
            return val == null ?
                   null :
                   $.isArray(val) ?
                   $.map(val, function (val, i) {
                       return { name : elem.name, value : val };
                   }) :
                   {
                       name : elem.name,
                       value : (o.checkboxesAsBools && this.type === 'checkbox') ? //moar ternaries!
                               (this.checked ? 'true' : 'false') :
                               val
                   };
        }).get();
};
