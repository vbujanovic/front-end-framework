# Routs
http_path = "/"
css_dir = "assets/styles"
sass_dir = "sass"
images_dir = "assets/images"
javascripts_dir = "assets/scripts"
fonts_dir = "assets/fonts"

# Options
output_style = :nested
relative_assets = true
line_comments = false

# Don't append query strings ('?2011051020102') to assets:
asset_cache_buster :none