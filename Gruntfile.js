module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({

        concat: {
            dist: {
                src: ['assets/scripts/libs/*.js', 'assets/scripts/helpers/*.js', 'assets/scripts/core/*.js', 'assets/scripts/pages/*.js', 'assets/scripts/components/*.js',  'assets/scripts/main/Master.js'],
                dest: 'assets/scripts/prod/scripts.js'
            }
        },

        uglify: {
            project_production: {
                files: {
                    'assets/scripts/prod/scripts.min.js': ['assets/scripts/prod/scripts.js']
                }
            }
        },

        watch: {
            files: ['assets/scripts/libs/*.js', 'assets/scripts/helpers/*.js', 'assets/scripts/core/*.js',  'assets/scripts/pages/*.js', 'assets/scripts/components/*.js', 'assets/scripts/main/Master.js'],
            tasks: ['concat', 'uglify']
        }

    });

    // Default task.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['concat', 'uglify']);

};